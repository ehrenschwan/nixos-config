{
  description = "My personal config";

  inputs = {
    # Apparently more nightly than arch!
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    nixpkgs-stable.url = "github:nixos/nixpkgs/nixos-24.05";
    disko.url = "github:nix-community/disko";
    home-manager.url = "github:nix-community/home-manager";
    agenix.url = "github:ryantm/agenix";
    neovim-nightly-overlay.url = "github:nix-community/neovim-nightly-overlay";
    nil.url = "github:oxalica/nil";
    arion.url = "github:hercules-ci/arion";
  };
  outputs =
    { nixpkgs, nixpkgs-stable, disko, home-manager, agenix, arion, ... }@inputs:
    let
      system = "x86_64-linux";
      overlays = [ inputs.neovim-nightly-overlay.overlays.default ];
      pkgs = import nixpkgs { inherit overlays system; };
    in {
      nixosConfigurations = {
        # this is growing out of proportion, i need a function for this
        tp = nixpkgs.lib.nixosSystem {
          inherit system;
          specialArgs = { inherit inputs; };
          modules = [
            ./hosts/tp/configuration.nix
            home-manager.nixosModules.home-manager
            {
              home-manager = {
                useUserPackages = true;
                useGlobalPkgs = true;
                users.ehrenschwan = ./hosts/tp/home.nix;
              };
            }
            agenix.nixosModules.default
            ({ nixpkgs.overlays = overlays; })
          ];
        };
        legion = nixpkgs.lib.nixosSystem {
          inherit system;
          specialArgs = { inherit inputs; };
          modules = [
            disko.nixosModules.disko
            (import ./shared/utils/disko.nix { device = "/dev/nvme0n1"; })
            ./hosts/legion/configuration.nix
            home-manager.nixosModules.home-manager
            {
              home-manager = {
                useUserPackages = true;
                useGlobalPkgs = true;
                users.ehrenschwan = ./hosts/legion/home.nix;
              };
            }
            ({ nixpkgs.overlays = overlays; })
          ];
        };
        installerIso = nixpkgs.lib.nixosSystem {
          inherit system;
          specialArgs = { inherit inputs; };
          modules = [
            ./hosts/installerIso/configuration.nix
            ({ nixpkgs.overlays = overlays; })
          ];
        };
        adguard = nixpkgs.lib.nixosSystem {
          inherit system;
          specialArgs = { inherit inputs; };
          modules = [
            disko.nixosModules.disko
            (import ./shared/utils/disko.nix { device = "/dev/sda"; })
            ./hosts/adguardhome/configuration.nix
          ];
        };
        hetzner = nixpkgs.lib.nixosSystem {
          inherit system;
          specialArgs = { inherit inputs; };
          modules = [
            disko.nixosModules.disko
            arion.nixosModules.arion
            (import ./shared/utils/disko.nix { device = "/dev/sda"; })
            ./hosts/hetzner/configuration.nix
          ];
        };
        docker = nixpkgs.lib.nixosSystem {
          inherit system;
          specialArgs = { inherit inputs; };
          modules = [
            disko.nixosModules.disko
            arion.nixosModules.arion
            (import ./shared/utils/disko.nix { device = "/dev/sda"; })
            agenix.nixosModules.default
            ./hosts/docker/configuration.nix
          ];
        };
        gitlab = nixpkgs.lib.nixosSystem {
          inherit system;
          specialArgs = { inherit inputs; };
          modules = [
            disko.nixosModules.disko
            (import ./shared/utils/disko.nix { device = "/dev/sda"; })
            agenix.nixosModules.default
            ./hosts/gitlab/configuration.nix
          ];
        };
      };
      devShells.${system}.default = pkgs.mkShell {
        buildInputs = with pkgs; [
          agenix.packages.${system}.default

          rust-analyzer
          rustfmt

          inputs.nil.packages.${system}.default
          nixfmt-classic

          stylua
          lua-language-server
          luajitPackages.luacheck

          shfmt
          shellcheck
          bash-language-server

          yamlfmt
        ];
      };
    };
}
