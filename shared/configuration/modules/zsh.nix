{ pkgs, config, lib, ... }:
with lib; {
  options = {
    eswn.zsh = {
      enable = mkOption {
        type = types.bool;
        default = false;
        description = "Enable zsh";
      };
    };
  };

  config = mkIf config.eswn.zsh.enable {
    programs.zsh.enable = true;
    users.defaultUserShell = pkgs.zsh;
  };
}
