{ config, lib, ... }:
with lib; {
  options = {
    eswn.printing = {
      enable = mkOption {
        type = types.bool;
        default = false;
        description = "Enable printing";
      };
    };
  };

  config = mkIf config.eswn.printing.enable {
    services.printing.enable = true;
    services.avahi = {
      enable = true;
      nssmdns4 = true;
      openFirewall = true;
    };
  };
}
