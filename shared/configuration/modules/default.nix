{ ... }: {
  imports = [
    ./amdgpu.nix
    ./de.nix
    ./ehrenschwan.nix
    ./emulation.nix
    ./greetd.nix
    ./grub-boot.nix
    ./networking.nix
    ./printing.nix
    ./root.nix
    ./systemd-boot.nix
    ./virtualisation.nix
    ./wireshark.nix
    ./zsh.nix
  ];
}
