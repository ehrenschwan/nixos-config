{ config, lib, ... }:
with lib; {
  options = {
    eswn.systemdBoot = {
      enable = mkOption {
        type = types.bool;
        default = false;
        description = "Enable systemd-boot";
      };
    };
  };

  config = mkIf config.eswn.systemdBoot.enable {
    boot.loader.systemd-boot.enable = true;
    boot.loader.efi.canTouchEfiVariables = true;
  };
}
