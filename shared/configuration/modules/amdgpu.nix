{ config, lib, ... }:
with lib; {
  options = {
    eswn.amdgpu = {
      enable = mkOption {
        type = types.bool;
        default = false;
        description = "Enable amdgpu";
      };
    };
  };

  config = mkIf config.eswn.amdgpu.enable {
    boot.initrd.kernelModules = [ "amdgpu" ];
    services.xserver.videoDrivers = [ "amdgpu" ];
  };
}
