{ pkgs, config, lib, ... }:
with lib; {
  options = {
    eswn.de = {
      enable = mkOption {
        type = types.bool;
        default = false;
        description = "Enable desktop environment";
      };
    };
  };

  config = mkIf config.eswn.de.enable {
    # For wayland to work in home-manager
    hardware.graphics.enable = true;

    # Audio configuration
    security.rtkit.enable = true;
    services.pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
    };
  };
}
