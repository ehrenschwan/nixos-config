{ pkgs, config, lib, ... }:
with lib; {
  options = {
    eswn.greetd = {
      enable = mkOption {
        type = types.bool;
        default = false;
        description = "Enable greetd";
      };
    };
  };

  config = mkIf config.eswn.greetd.enable {
    services.greetd = {
      enable = true;
      settings = {
        default_session = {
          command =
            "${pkgs.greetd.tuigreet}/bin/tuigreet --time --cmd Hyprland";
        };
      };
    };
  };
}
