{ config, lib, ... }:
with lib; {
  options = {
    eswn.wireshark = {
      enable = mkOption {
        type = types.bool;
        default = false;
        description = "Enable wireshark";
      };
    };
  };

  config =
    mkIf config.eswn.wireshark.enable { programs.wireshark.enable = true; };
}
