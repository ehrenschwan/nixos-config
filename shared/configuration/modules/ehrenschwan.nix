{ config, lib, ... }:
with lib; {
  options = {
    eswn.user.ehrenschwan = {
      enable = mkOption {
        type = types.bool;
        default = false;
        description = "Enable user ehrenschwan";
      };
      authorizedKeys = mkOption {
        type = types.listOf types.str;
        default = [ ];
        description = "List of authorized keys";
      };
    };
  };

  config = mkIf config.eswn.user.ehrenschwan.enable {
    users.users.ehrenschwan = {
      isNormalUser = true;
      description = "ehrenschwan";
      extraGroups = [ "networkmanager" "wheel" ]
        ++ lib.optionals config.eswn.virtualisation.containers [ "podman" ]
        ++ lib.optionals config.eswn.virtualisation.vms [ "libvirtd" ]
        ++ lib.optionals config.eswn.wireshark.enable [ "wireshark" ];
      openssh.authorizedKeys.keys = config.eswn.user.ehrenschwan.authorizedKeys;
    };
  };
}
