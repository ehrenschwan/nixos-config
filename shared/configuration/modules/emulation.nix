{ lib, config, ... }:
with lib; {
  options = {
    eswn.emulation.aarch = {
      enable = mkOption {
        type = types.bool;
        default = false;
        description = "Enable aarch64 emulation";
      };
    };
  };

  config = mkIf config.eswn.emulation.aarch.enable {
    boot.binfmt.emulatedSystems = [ "aarch64-linux" ];
  };
}
