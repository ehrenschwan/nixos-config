{ lib, config, pkgs, ... }:
with lib; {
  options = {
    eswn.virtualisation = {
      containers = mkOption {
        type = types.bool;
        default = false;
        description = "Enable containers";
      };
      vms = mkOption {
        type = types.bool;
        default = false;
        description = "Enable libvirtd";
      };
    };
  };

  config = {
    virtualisation.docker.enable = false;
    virtualisation.podman = lib.mkIf config.eswn.virtualisation.containers {
      enable = true;
      dockerCompat = true;
      dockerSocket.enable = true;
      defaultNetwork.settings.dns_enabled = true;
    };
    environment.systemPackages = [ ]
      ++ lib.optionals config.eswn.virtualisation.containers
      [ pkgs.podman-compose ];
    virtualisation.libvirtd.enable = config.eswn.virtualisation.vms;
    programs.virt-manager.enable = config.eswn.virtualisation.vms;
  };
}
