{ config, lib, ... }:
with lib; {
  options = {
    eswn.grubBoot = {
      enable = mkOption {
        type = types.bool;
        default = false;
        description = "Enable grub boot";
      };
    };
  };

  config = mkIf config.eswn.systemdBoot.enable {
    boot.loader.grub = {
      efiSupport = true;
      efiInstallAsRemovable = true;
    };
  };
}
