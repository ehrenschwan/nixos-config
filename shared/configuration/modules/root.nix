{ config, lib, ... }:
with lib; {
  options = {
    eswn.user.root = {
      authorizedKeys = mkOption {
        type = types.listOf types.str;
        default = [ ];
        description = "List of authorized keys";
      };
    };
  };

  config = {
    users.users.root = {
      extraGroups = [ ]
        ++ lib.optionals config.eswn.virtualisation.containers [ "podman" ]
        ++ lib.optionals config.eswn.virtualisation.vms [ "libvirtd" ];
      openssh.authorizedKeys.keys = config.eswn.user.root.authorizedKeys;
    };
  };
}
