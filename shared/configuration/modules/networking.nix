{ config, lib, ... }:
with lib; {
  options = {
    eswn = {
      host = mkOption {
        type = types.str;
        default = "host";
        description = "The networking hostname";
      };
      allowedTCPPorts = mkOption {
        type = types.listOf types.int;
        default = [ ];
        description = "List of allowed TCP ports";
      };
      allowedUDPPorts = mkOption {
        type = types.listOf types.int;
        default = [ ];
        description = "List of allowed UDP ports";
      };
    };
  };

  config = {
    networking = {
      hostName = "${config.eswn.host}-nixos";
      networkmanager.enable = true;
      firewall = {
        enable = true;
        allowedTCPPorts = config.eswn.allowedTCPPorts;
        allowedUDPPorts = config.eswn.allowedUDPPorts;
      };
    };
  };
}
