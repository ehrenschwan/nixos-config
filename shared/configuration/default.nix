{ ... }: {
  imports = [ ./modules ];

  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  # Locale
  time.timeZone = "Europe/Berlin";
  i18n.defaultLocale = "en_US.UTF-8";
  i18n.extraLocaleSettings = {
    LC_ADDRESS = "de_DE.UTF-8";
    LC_IDENTIFICATION = "de_DE.UTF-8";
    LC_MEASUREMENT = "de_DE.UTF-8";
    LC_MONETARY = "de_DE.UTF-8";
    LC_NAME = "de_DE.UTF-8";
    LC_NUMERIC = "de_DE.UTF-8";
    LC_PAPER = "de_DE.UTF-8";
    LC_TELEPHONE = "de_DE.UTF-8";
    LC_TIME = "de_DE.UTF-8";
  };

  # SSH
  services.openssh = {
    enable = true;
    settings.PasswordAuthentication = false;
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # FIXME: Has to be here for now hope this get's fixed soon
  nixpkgs.config.permittedInsecurePackages = [ "electron-27.3.11" ];

  # This can probably stay how it is, but checkout the documentation before changing.
  system.stateVersion = "24.05";
}
