-- general settings
require("settings")

-- lazy
require("lazy-setup")

-- uses telescope so needs to come after plugins
require("mapping.maps")

-- language server setup
require("lsp-config.language-servers")

-- includes formatting and linting
require("autocmds")

-- snippets
require("snippets")
