local helper = require("mapping.helper")
local map = helper.map

-- for more keybinds see plugins/lspsaga.lua & plugins/trouble.lua
local on_attach = function(_, bufnr)
	map("n", "gd", vim.lsp.buf.definition, "Go to definition", bufnr)
	map("i", "<C-h>", vim.lsp.buf.signature_help, "Show signature", bufnr)
	map("n", "<leader>li", vim.lsp.buf.implementation, "Show implementations", bufnr)
	map("n", "<leader>le", vim.diagnostic.open_float, "Open float", bufnr)
	map("n", "<leader>lh", function()
		local curr_buff = vim.api.nvim_get_current_buf()
		if vim.lsp.inlay_hint.is_enabled() then
			vim.lsp.inlay_hint.enable(curr_buff, false)
		else
			vim.lsp.inlay_hint.enable(curr_buff, true)
		end
	end)
end

vim.diagnostic.config({
	signs = {
		text = {
			[vim.diagnostic.severity.ERROR] = " ",
			[vim.diagnostic.severity.WARN] = " ",
			[vim.diagnostic.severity.INFO] = " ",
			[vim.diagnostic.severity.HINT] = " ",
		},
	},
})

local lsp = require("lspconfig")
local lsp_flags = {
	debounce_text_changes = 150,
}

-- lua
lsp.lua_ls.setup({
	on_attach = on_attach,
	flags = lsp_flags,
	cmd = { "lua-language-server" },
	settings = {
		Lua = {
			hint = {
				enable = true,
			},
			-- Do not send telemetry data containing a randomized but unique identifier
			telemetry = {
				enable = false,
			},
			diagnostics = {
				disable = { "missing-fields" },
			},
		},
	},
})

lsp.svelte.setup({
	on_attach = on_attach,
	flags = lsp_flags,
	settings = {
		svelte = {
			plugin = {
				scss = {},
				svelte = {
					compilerWarnings = {
						cssUnusedSelectors = "ignore",
					},
				},
			},
		},
	},
})

lsp.tailwindcss.setup({
	on_attach = on_attach,
	flags = lsp_flags,
	settings = {
		filetypes = {
			"html",
			"css",
			"scss",
			"javascript",
			"javascriptreact",
			"typescript",
			"typescriptreact",
			"svelte",
		},
	},
})

local servers = { "tsserver", "jsonls", "cssls", "yamlls", "nil_ls", "bashls" }
for _, server in ipairs(servers) do
	lsp[server].setup({
		flags = lsp_flags,
		on_attach = on_attach,
	})
end
