local helper = require("mapping.helper")
local map = helper.map
local cmd = helper.cmd

-- window
map("n", "<leader>w", "<C-w>", "Window prefix")

-- basic editing
map("n", "<leader>s", cmd("w"), "Save file")
map("n", "<leader>cr", cmd("source $MYVIMRC"), "Reload config")
map("n", "<CR>", cmd("nohlsearch"), "Clear search highlights")

-- searching enhancement
vim.keymap.set("c", "<space>", function()
	local mode = vim.fn.getcmdtype()
	if mode == "?" or mode == "/" then
		return ".*"
	else
		return " "
	end
end, { expr = true })

-- tabs
map("n", "<leader>tn", function()
	vim.notify("Creating new tab", vim.log.levels.INFO, { title = "Tab" })
	vim.cmd("tabnew")
end, "New tab")
map("n", "<leader>tc", function()
	vim.notify("Closing tab", vim.log.levels.INFO, { title = "Tab" })
	vim.cmd("tabclose")
end, "Close tab")
