local helper = {}

function helper.map(m, k, v, desc, bufnr)
	vim.keymap.set(m, k, v, { noremap = true, silent = true, desc = desc, buffer = bufnr })
end

function helper.cmd(command)
	return string.format("<cmd>%s<CR>", command)
end

return helper
