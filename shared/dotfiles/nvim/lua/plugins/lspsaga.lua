return {
	-- revert back to "nvimdev/lspsaga.nvim" when https://github.com/nvimdev/lspsaga.nvim/pull/1398 is merged
	"scratchyone/lspsaga.nvim",
	opts = {
		lightbulb = {
			sign = false,
		},
		hover = {
			open_cmd = "!firefox",
		},
		rename = {
			keys = {
				quit = "<esc>",
			},
		},
		ui = {
			code_action = "",
		},
	},
	config = function(_, opts)
		require("lspsaga").setup(opts)

		local helper = require("mapping.helper")
		local map = helper.map
		local cmd = helper.cmd

		map("n", "<leader>lc", cmd("Lspsaga code_action"), "Opens Lspsaga code action")
		map("n", "[d", cmd("Lspsaga diagnostic_jump_prev"), "Previous Lspsaga diagnostic")
		map("n", "]d", cmd("Lspsaga diagnostic_jump_next"), "Next Lspsaga diagnostic")
		map("n", "H", cmd("Lspsaga hover_doc"), "Hover docs")
		map("n", "<leader>lr", cmd("Lspsaga rename"), "Rename symbol")
	end,
	dependencies = {
		"nvim-treesitter/nvim-treesitter",
		"nvim-tree/nvim-web-devicons",
	},
}
