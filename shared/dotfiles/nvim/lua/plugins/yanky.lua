return {
	"gbprod/yanky.nvim",
	config = function()
		require("yanky").setup({
			ring = {
				history_length = 100,
				storage = "shada",
				sync_with_numbered_registers = true,
				cancel_event = "update",
				ignore_registers = { "_" },
			},
			highlight = {
				on_yank = true,
				on_put = true,
				timer = 100,
			},
		})

		local helper = require("mapping.helper")
		local map = helper.map

		map({ "n", "x" }, "p", "<Plug>(YankyPutAfter)")
		map({ "n", "x" }, "P", "<Plug>(YankyPutBefore)")
		map({ "n", "x" }, "gp", "<Plug>(YankyGPutAfter)")
		map({ "n", "x" }, "gP", "<Plug>(YankyGPutBefore)")
		map("n", "<c-n>", "<Plug>(YankyCycleForward)")
		map("n", "<c-p>", "<Plug>(YankyCycleBackward)")
	end,
}
