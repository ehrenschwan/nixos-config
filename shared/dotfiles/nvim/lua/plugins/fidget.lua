return {
	"j-hui/fidget.nvim",
	opts = {
		notification = {
			override_vim_notify = true,
			window = {
				winblend = 0, -- note: not winblend!
				relative = "editor",
			},
			view = {
				group_separator = " ",
			},
		},
	},
}
