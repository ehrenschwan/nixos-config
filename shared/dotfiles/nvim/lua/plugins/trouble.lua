return {
	"folke/trouble.nvim",
	dependencies = { "nvim-tree/nvim-web-devicons" },
	opts = {},
	cmd = "Trouble",
	config = function()
		local helper = require("mapping.helper")
		local map = helper.map

		local trouble = require("trouble")

		map("n", "<leader>dw", function()
			trouble.toggle("diagnostics")
		end, "Open trouble workspace diagnostics")
		map("n", "<leader>dq", function()
			trouble.toggle("qflist")
		end, "Open trouble quickfix")
		map("n", "gr", function()
			trouble.toggle("lsp_references")
		end, "Open trouble lsp references")
	end,
}
