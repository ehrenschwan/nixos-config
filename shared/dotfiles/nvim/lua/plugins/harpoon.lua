return {
	"ThePrimeagen/harpoon",
	config = function()
		local helper = require("mapping.helper")
		local map = helper.map

		local ui = require("harpoon.ui")
		local mark = require("harpoon.mark")

		map("n", "<leader>ha", mark.add_file, "Add file to harpoon")
		map("n", "<leader>hl", ui.toggle_quick_menu)

		map("n", "<leader>a", function()
			ui.nav_file(1)
		end, "Harpoon first file")
		map("n", "<leader>r", function()
			ui.nav_file(2)
		end, "Harpoon second file")
		map("n", "<leader>i", function()
			ui.nav_file(3)
		end, "Harpoon third file")
		map("n", "<leader>o", function()
			ui.nav_file(4)
		end, "Harpoon fourth file")
	end,
}
