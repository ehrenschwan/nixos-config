return {
	{
		"nvim-treesitter/nvim-treesitter",
		config = function()
			require("nvim-treesitter.configs").setup({
				sync_install = false,
				auto_install = true,
				ensure_installed = {
					"lua",
					"nix",
					"rust",
					"typescript",
					"scss",
					"svelte",
					"yaml",
				},
				highlight = {
					enable = true,
					additional_vim_regex_highlighting = false,
				},
				textobjects = {
					select = {
						enable = true,
						lookahead = true,
						keymaps = {
							["af"] = "@function.outer",
							["if"] = "@function.inner",
							["ac"] = "@class.outer",
							["ic"] = "@class.inner",
							["ii"] = "@conditional.inner",
							["ai"] = "@conditional.outer",
							["al"] = "@loop.outer",
							["il"] = "@loop.inner",
						},
					},
					move = {
						enable = true,
						set_jumps = true, -- whether to set jumps in the jumplist
						goto_next_start = {
							["]]"] = "@function.outer",
							["]m"] = "@class.outer",
							["]l"] = "@loop.*",
						},
						goto_next_end = {
							["]["] = "@function.outer",
							["]M"] = "@class.outer",
						},
						goto_previous_start = {
							["[["] = "@function.outer",
							["[m"] = "@class.outer",
						},
						goto_previous_end = {
							["[]"] = "@function.outer",
							["[M"] = "@class.outer",
						},
						goto_next = {
							["]c"] = "@conditional.outer",
						},
						goto_previous = {
							["[c"] = "@conditional.outer",
						},
					},
				},
			})
		end,
	},
	"nvim-treesitter/nvim-treesitter-refactor",
	{
		"nvim-treesitter/nvim-treesitter-textobjects",
		deps = "nvim-treesitter/nvim-treesitter",
		after = "nvim-treesitter",
	},
	"nvim-treesitter/nvim-treesitter-context",
}
