return {
	"tamton-aquib/staline.nvim",
	main = "staline",
	dependencies = {
		"ellisonleao/gruvbox.nvim",
	},
	opts = {
		defaults = {
			left_separator = "",
			right_separator = "",
			cool_symbol = " ",
			full_path = false,
			mod_symbol = "  ",
			lsp_client_symbol = " ",
			line_column = "[%l/%c]  %p%% ",
			fg = "#000000",
			bg = "#3C3836",
			inactive_color = "#303030",
			inactive_bgcolor = "none",
			true_colors = true,
			font_active = "none",
			branch_symbol = "",
		},
		mode_icons = {
			n = "󰋜",
			i = "",
			c = "",
			v = "󰈈",
			V = "",
		},
		sections = {
			left = { "-mode", "left_sep_double", "-branch" },
			mid = { "file_name" },
			right = { "right_sep_double", "-line_column" },
		},
		lsp_symbols = { Error = " ", Info = " ", Warn = " ", Hint = "" },
	},
}
