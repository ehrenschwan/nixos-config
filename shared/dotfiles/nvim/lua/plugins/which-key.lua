return {
	"folke/which-key.nvim",
	event = "VeryLazy",
	deps = "mini.icons",
	init = function()
		vim.o.timeout = true
		vim.o.timeoutlen = 300
	end,
	opts = {},
}
