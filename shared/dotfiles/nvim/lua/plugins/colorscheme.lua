return {
	"ellisonleao/gruvbox.nvim",
	lazy = false,
	priority = 1000,
	opts = {
		italic = {
			comments = true,
		},
		transparent_mode = true,
		undercurl = true,
		underline = true,
	},
	config = function(_, opts)
		require("gruvbox").setup(opts)
		vim.cmd([[colorscheme gruvbox]])
		vim.cmd([[hi CursorLineNr guibg=NONE guifg=NONE]])
		vim.cmd([[hi CursorLine guibg=#1d2021 guifg=NONE]])
	end,
}
