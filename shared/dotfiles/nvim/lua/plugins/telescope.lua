return {
	"nvim-telescope/telescope.nvim",
	tag = "0.1.4",
	dependencies = {
		"nvim-lua/plenary.nvim",
		"folke/trouble.nvim",
		"LukasPietzschmann/telescope-tabs",
	},
	opts = function()
		local actions = require("telescope.actions")
		local trouble = require("trouble.sources.telescope")

		return {
			defaults = {
				mappings = {
					i = { ["<C-t>"] = trouble.open },
					n = {
						["n"] = actions.move_selection_next,
						["e"] = actions.move_selection_previous,
					},
				},
			},
			pickers = {
				find_files = {
					-- the ever growing list of ignored directories :)
					find_command = { "rg", "--files", "-uu", "-g", "!{.git,node_modules,.obsidian,.trash,.cache}/*" },
				},
			},
		}
	end,
	config = function(_, opts)
		local telescope = require("telescope")
		telescope.setup(opts)

		telescope.load_extension("yank_history")
		telescope.load_extension("telescope-tabs")

		local helper = require("mapping.helper")
		local map = helper.map

		local builtin = require("telescope.builtin")

		map("n", "<leader>ff", builtin.git_files, "Find files")
		map("n", "<leader>fa", builtin.find_files, "Find all files")
		map("n", "<leader>fb", builtin.buffers, "Find all buffers")
		map("n", "<leader>fd", builtin.diagnostics, "Show all diagnostics")
		map("n", "<leader>fw", builtin.live_grep, "Ripgrep")
		map("n", "<leader>fy", telescope.extensions.yank_history.yank_history, "Show yank history")
		map("n", "<leader>fh", builtin.help_tags, "Show help tags")
		map("n", "<leader>tt", require("telescope-tabs").list_tabs, "Show current buffer tags")
	end,
}
