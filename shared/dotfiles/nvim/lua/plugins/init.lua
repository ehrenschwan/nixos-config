return {
	"neovim/nvim-lspconfig",

	"lambdalisue/suda.vim",

	-- folke goodies
	{
		"folke/todo-comments.nvim",
		dependencies = { "nvim-lua/plenary.nvim" },
		opts = {},
	},

	-- text display
	"HiPhish/rainbow-delimiters.nvim",
	{ "numToStr/Comment.nvim", opts = {} },

	-- languages
	"leafOfTree/vim-svelte-plugin",
	"NoahTheDuke/vim-just",

	-- -- my own plugins
	-- {
	-- 	dir = "~/dev/nvim_plugin_test/",
	-- 	lazy = true,
	-- 	ft = "markdown",
	-- 	config = function()
	-- 		require("nvim_plugin_test")
	-- 	end,
	-- },
}
