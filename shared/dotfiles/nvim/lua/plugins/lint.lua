return {
	"mfussenegger/nvim-lint",
	config = function()
		local lint = require("lint")
		lint.linters.luacheck = {
			cmd = "luacheck",
			stdin = true,
			args = { "--globals", "vim", "--formatter", "plain" },
			stream = "stdout",
			ignore_exitcode = true,
			parser = require("lint.parser").from_pattern(
				[[(%d+):(%d+):(%w+):(.+)]],
				{ "row", "col", "type", "message" }
			),
		}

		lint.linters_by_ft = {
			javascript = { "eslint_d" },
			typescript = { "eslint_d" },
			svelte = { "eslint_d" },
			nix = { "nix" },
			sh = { "shellcheck" },
			bash = { "shellcheck" },
		}
	end,
}
