return {
	"stevearc/oil.nvim",
	dependencies = { "nvim-tree/nvim-web-devicons" },
	opts = {
		keymaps = {
			["L"] = "actions.select",
			["H"] = "actions.parent",
		},
		columns = {
			"icon",
		},
		view_options = {
			show_hidden = true,
		},
	},
	config = function(_, opts)
		require("oil").setup(opts)

		local helper = require("mapping.helper")
		local map = helper.map
		local cmd = helper.cmd

		map("n", "<leader>fo", cmd("Oil"), "Oil.nvim")
	end,
}
