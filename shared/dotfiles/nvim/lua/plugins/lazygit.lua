return {
	"kdheepak/lazygit.nvim",
	config = function()
		local helper = require("mapping.helper")
		local map = helper.map

		map("n", "<leader>g", "<cmd>LazyGit<CR>", "Open LazyGit")
	end,
}
