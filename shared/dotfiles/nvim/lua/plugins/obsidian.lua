return {
	"epwalsh/obsidian.nvim",
	version = "*",
	lazy = true,
	ft = "markdown",
	dependencies = {
		"nvim-lua/plenary.nvim",
	},
	opts = {
		workspaces = {
			{
				name = "main",
				path = "~/obsidian/main",
			},
		},
		templates = {
			subdir = "templates",
			date_format = "%Y-%m-%d",
			time_format = "%H:%M",
		},
	},
	config = function(_, opts)
		require("obsidian").setup(opts)

		local helper = require("mapping.helper")
		local map = helper.map
		local cmd = helper.cmd

		map("n", "<leader>on", cmd("edit"), "Obsidian: New note")
		map("n", "<leader>ot", cmd("ObsidianTags"), "Obsidian: Tags")
		map("n", "<leader>ob", cmd("ObsidianBacklinks"), "Obsidian: Backlinks")
		map("n", "<leader>oi", cmd("ObsidianTemplate"), "Obsidian: Insert template")
	end,
}
