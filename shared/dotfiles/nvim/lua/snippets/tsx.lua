local ls = require("luasnip")

local s = ls.snippet
local t = ls.text_node
local i = ls.insert_node
local d = ls.dynamic_node
local sn = ls.snippet_node
-- local f = ls.function_node
-- local c = ls.choice_node

return {
	s({ trig = "err", name = "openapi-fetch-error", desc = "openapi-fetch error" }, {
		t("if ("),
		i(1),
		t({ " !== undefined) {", "  console.error(" }),
		d(2, function(args)
			return sn(nil, {
				i(1, args[1]),
			})
		end, { 1 }),
		t({ ");", "  return json({ error: errorToMessage(" }),
		d(3, function(args)
			return sn(nil, {
				i(1, args[1]),
			})
		end, { 1 }),
		t({ ", t) });", "}" }),
	}),
}
