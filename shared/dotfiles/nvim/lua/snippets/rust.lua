local ls = require("luasnip")

local fmt = require("luasnip.extras.fmt").fmta
local s = ls.snippet
local t = ls.text_node
local i = ls.insert_node
-- local f = ls.function_node
-- local n = ls.snippet_node
-- local c = ls.choice_node
-- local d = ls.dynamic_node

return {
	s(
		{ trig = "fn", name = "function", desc = "Function" },
		fmt(
			[[
      fn <>(<>) {
        <>
      }
    ]],
			{ i(1), i(2), i(0) }
		)
	),
	s(
		{ trig = "struct", name = "struct", desc = "Struct" },
		fmt(
			[[
      struct <> {
        <>
      }
    ]],
			{ i(1), i(0) }
		)
	),
	s(
		{ trig = "enum", name = "enum", desc = "Enum" },
		fmt(
			[[
      enum <> {
        <>
      }
    ]],
			{
				i(1),
				i(0),
			}
		)
	),
	s(
		{ trig = "impl", name = "impl", desc = "Impl" },
		fmt(
			[[
      impl <> {
        <>
      }
    ]],
			{
				i(1),
				i(0),
			}
		)
	),
	s(
		{ trig = "match", name = "match", desc = "Match" },
		fmt(
			[[
      match <> {
        <>
      }
    ]],
			{
				i(1),
				i(0),
			}
		)
	),
	s(
		{ trig = "if", name = "if", desc = "If" },
		fmt(
			[[
      if <> {
        <>
      }
    ]],
			{
				i(1),
				i(2),
			}
		)
	),
	s(
		{ trig = "ifl", name = "if let", desc = "If let" },
		fmt(
			[[
      if let <> = <> {
        <>
      }
    ]],
			{
				i(1),
				i(2),
				i(0),
			}
		)
	),
	s(
		{ trig = "for", name = "for", desc = "For" },
		fmt(
			[[
      for <> in <> {
        <>
      }
    ]],
			{
				i(1),
				i(2),
				i(0),
			}
		)
	),
	s(
		{ trig = "der", name = "derive", desc = "Derive" },
		fmt("#[derive(<>)]", {
			i(1),
		})
	),
	s(
		{ trig = "test", name = "test", desc = "Test" },
		fmt(
			[[
      #[rstest]
      fn <>(<>) {
        <>
      }
  ]],
			{ i(1), i(2), i(0) }
		)
	),
	s("case", fmt("#[case<>(<>)]", { i(1), i(0) })),
	s(
		"mtest",
		fmt(
			[[
      #[cfg(test)]
      mod tests {
        use rstest::rstest;

        use super::*;

        #[rstest]
        <>
        fn <>(<>) {
          <>
        }
      }
  ]],
			{ i(1), i(2), i(3), i(0) }
		)
	),
	s("dbg", fmt("#[derive(Debug, <>)]", { i(1) })),
	s("print", fmt('println!("<>", <>)', { i(1), i(0) })),
	s(
		"dprint",
		fmt('println!("{:#?}", <>)', {
			i(0),
		})
	),
}
