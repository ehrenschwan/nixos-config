local helper = require("mapping.helper")
local map = helper.map
local cmd = helper.cmd

map("n", "<leader><leader>s", cmd("source ~/.config/nvim/lua/snippets/init.lua"), "Resource snippets file")

if not table.unpack then
	table.unpack = unpack
end

local ls = require("luasnip")

local s = ls.snippet
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
-- local n = ls.snippet_node
-- local c = ls.choice_node
-- local d = ls.dynamic_node

local date = function()
	return os.date("%Y-%m-%d")
end

local all = {
	s({ trig = "date", name = "date", desc = "Date in the form of YYYY-MM-DD" }, { f(date, {}) }),
}

local js = {
	s({ trig = "imp", name = "import", desc = "Import statement" }, {
		t("import "),
		i(1),
		t(" from '"),
		i(2),
		t("';"),
	}),
	s({ trig = "cl", name = "console_log", desc = "Console log" }, {
		t("console.log("),
		i(1),
		t(");"),
	}),
}

local ts = {
	table.unpack(js),
	s({ trig = "int", name = "interface", desc = "Interface" }, {
		t("interface "),
		i(1),
		t({ " {", "  " }),
		i(2),
		t("}"),
	}),
	s({ trig = "type", name = "type", desc = "Type" }, {
		t("type "),
		i(1),
		t({ " = {", "  " }),
		i(2),
		t({ "", "}" }),
	}),
}

local svelte = {
	s({ trig = "script", name = "script", desc = "Svelte script tag" }, {
		t({ '<script lang="ts">', "  " }),
		i(1),
		t({ "", "</script>" }),
	}),
	s({ trig = "style", name = "style", desc = "Svelte style tag" }, {
		t({ '<style lang="scss">', "  " }),
		i(1),
		t({ "", "</style>" }),
	}),
}

ls.add_snippets(nil, {
	all = all,
	javascript = js,
	typescript = ts,
	svelte = svelte,
	rust = require("snippets.rust"),
	typescriptreact = require("snippets.tsx"),
})
