{ pkgs, config, lib, ... }:
with lib; {
  options = {
    eswn.cli.git = {
      userName = mkOption {
        type = types.str;
        default = "ehrenschwan";
      };
      userEmail = mkOption {
        type = types.str;
        default = "luca@ehrenschwan.dev";
      };
    };
  };

  config = {
    home.packages = with pkgs; [
      # for compiling C, and because I think it's cool
      zig
      nodejs_latest
      nodejs_latest.pkgs.typescript
      nodejs_latest.pkgs.pnpm
      nodejs_latest.pkgs.yarn

      # lsps
      svelte-language-server
      tailwindcss-language-server
      # this is missing cssls, yamlls, jsonls, and typescriptls

      eslint_d
      prettierd

      lua
      luarocks
      postgresql
      just
      jq
      ansible
      graphviz
    ];

    programs.git = {
      enable = true;
      userName = config.eswn.cli.git.userName;
      userEmail = config.eswn.cli.git.userEmail;
      extraConfig = {
        init = { defaultBranch = "main"; };
        pull = { rebase = true; };
        rerere.enabled = true;
      };
    };

    programs.neovim = {
      enable = true;
      defaultEditor = true;
      package = pkgs.neovim;
    };
  };
}
