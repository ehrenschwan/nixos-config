{ pkgs, ... }: {
  fonts.fontconfig.enable = true;

  home.packages = with pkgs; [
    #fonts
    (nerdfonts.override { fonts = [ "JetBrainsMono" ]; })
    hunspell
    hunspellDicts.de_DE
  ];
}

