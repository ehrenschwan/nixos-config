{ ... }: {
  programs.starship = {
    enable = true;
    enableZshIntegration = true;
    settings = {
      format = "$directory$git_branch$git_status$hostname$line_break$character";
      right_format = "$rust$node$nix_shell";
      hostname = {
        ssh_only = true;
        ssh_symbol = "🌐";
        format = "[$ssh_symbol$hostname]($style) ";
      };
      directory = {
        format = "[$path](bold purple)[$read_only]($read_only_style) ";
      };
      git_branch = {
        symbol = "";
        format = "[$symbol$branch](bold blue) ";
      };
      line_break = { disabled = false; };
      character = {
        success_symbol = "[\\$](bold green)";
        error_symbol = "[x](bold red)";
      };
    };
  };
}
