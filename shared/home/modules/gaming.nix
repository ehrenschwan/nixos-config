{ pkgs, config, lib, ... }:
with lib; {
  options = {
    eswn.de.gaming = {
      enable = mkOption {
        type = types.bool;
        default = false;
        description = "Enable gaming";
      };
    };
  };

  config = mkIf config.eswn.de.gaming.enable {
    home.packages = with pkgs; [
      # Minecraft
      prismlauncher
      steam
      bottles
    ];
  };
}
