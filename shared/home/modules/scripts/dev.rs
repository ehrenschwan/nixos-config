#!/usr/bin/env -S nix shell "github:nix-community/fenix?rev=092bd452904e749efa39907aa4a20a42678ac31e#minimal.toolchain" -c cargo -q -Zscript
//! ```cargo
//! [package]
//! name = "dev"
//! version = "1.0.6"
//!
//! [dependencies]
//! clap = { version = "4.5.7", features = ["derive"] }
//! anyhow = "1.0.86"
//! home = "0.5.9"
//! ```

use anyhow::{Context, Result};
use clap::{Parser, Subcommand};
use std::env;
use std::path::Path;
use std::process::Command;

#[derive(Parser, Debug)]
struct Args {
    #[command(subcommand)]
    command: DevCommand,
}

#[derive(Subcommand, Debug)]
enum DevCommand {
    /// Build the project
    Start {
        // The project to start
        project: String,
    },
    /// List all projects
    #[clap(alias = "ls")]
    List,
    /// Clean the sessions
    Clean,
}

fn cmd(args: &[&str]) -> Result<()> {
    let mut cmd = Command::new(args[0]);
    cmd.args(&args[1..]);
    let status = cmd.status()?;
    if !status.success() {
        anyhow::bail!("Command failed with status: {}", status);
    }
    Ok(())
}

fn main() -> Result<()> {
    let args = Args::parse();
    let home_dir = home::home_dir().context("Failed to get home directory")?;
    let home = home_dir
        .to_str()
        .context("Failed to convert home directory to string")?;

    match args.command {
        DevCommand::Start { mut project } => {
            if std::env::var("ZELLIJ").is_ok() {
                anyhow::bail!("Zellij is already running, please exit first");
            }

            let mut project_dir = format!("{}/", home);
            match project.as_str() {
                "config" => {
                    project = "nixos-config".to_string();
                    project_dir.push_str("nixos-config/");
                }
                "obsidian" => project_dir.push_str("obsidian/main/"),
                _ => project_dir.push_str(&format!("dev/{}/", project)),
            }

            let project_path = Path::new(&project_dir);

            let output = Command::new("zellij").args(&["ls", "--short"]).output()?;

            let sessions = String::from_utf8(output.stdout)?;

            if sessions.as_str().lines().any(|p| p == &project) {
                println!("WARN: zellij session for that project already running, connecting...");
                cmd(&["zellij", "a", &project])?;
                return Ok(());
            }

            env::set_current_dir(project_path)?;

            if Path::new(".envrc").exists() {
                println!("Loading .envrc file");
                let _ = cmd(&["direnv", "export", "zsh"]);
            }

            println!("Starting zellij session for {}...", project);
            cmd(&["zellij", "-s", &project, "-l", "dev"])?;
        }
        DevCommand::List => {
            println!("Available Projects: ");
            println!("  config");
            println!("  obsidian");
            let projects = std::fs::read_dir(format!("{}/dev", home))?;
            projects.for_each(|project| {
                if let Ok(project) = project {
                    if project.file_type().unwrap().is_dir() {
                        let project_name = project.file_name().into_string().unwrap();
                        println!("  {}", project_name);
                    }
                }
            });
        }
        DevCommand::Clean => {
            cmd(&["zellij", "kill-all-sessions"])?;
            cmd(&["zellij", "delete-all-sessions"])?;
        }
    }

    Ok(())
}
