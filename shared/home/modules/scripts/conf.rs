#!/usr/bin/env -S nix shell "github:nix-community/fenix?rev=092bd452904e749efa39907aa4a20a42678ac31e#minimal.toolchain" -c cargo -q -Zscript
//! ```cargo
//! [package]
//! name = "conf"
//! version = "1.0.11"
//!
//! [dependencies]
//! clap = { version = "4.5.7", features = ["derive"] }
//! anyhow = "1.0.86"
//! ```

use anyhow::Result;
use clap::{Parser, Subcommand};
use std::{collections::VecDeque, process::Command};

#[derive(Parser, Debug)]
#[command(version, about)]
struct Args {
    #[command(subcommand)]
    command: ConfCommand,
}

#[derive(Debug, Subcommand)]
enum ConfCommand {
    /// Apply new configuration
    Switch {
        /// Host configuration to apply
        host: String,
        /// Remote target
        #[arg(short, long)]
        remote: Option<String>,
    },
    /// Update the configuration flake
    Update,
    /// Clean the system
    Clean,
    /// Build an iso image
    Iso,
}

fn sudo(args: &[&str]) -> Result<()> {
    let mut cmd = Command::new("sudo");
    cmd.args(args);
    let status = cmd.status()?;
    if !status.success() {
        anyhow::bail!("Command failed with status: {}", status);
    }
    Ok(())
}

fn main() -> Result<()> {
    let args = Args::parse();

    match args.command {
        ConfCommand::Switch { host, remote } => {
            println!("Applying {} configuration", host);
            let flake = format!("/home/ehrenschwan/nixos-config#{}", host);
            let mut cmds: VecDeque<_> = VecDeque::from(["switch", "--flake", &flake]);

            let host: String;
            if let Some(target_host) = remote {
                host = format!("root@{}", target_host);
                cmds.push_back("--target-host");
                cmds.push_back(&host);

                let mut cmd = Command::new("nixos-rebuild");
                cmd.args(cmds);
                let status = cmd.status()?;
                if !status.success() {
                    anyhow::bail!("Command failed with status: {}", status);
                }
            } else {
                cmds.push_front("nixos-rebuild");
                let cmds_vec = Vec::from(cmds);
                sudo(&cmds_vec)?;
            }
        }
        ConfCommand::Update => {
            println!("Updating configuration flake");
            sudo(&["nix", "flake", "update"])?;
        }
        ConfCommand::Clean => {
            println!("Cleaning the system");
            sudo(&["nix-env", "--delete-generations", "old"])?;
            sudo(&["nix-store", "--gc"])?;
            sudo(&["nix-channel", "--update"])?;
            sudo(&["nix-env", "-u", "--always"])?;
        }
        ConfCommand::Iso => {
            println!("Building iso image");
            sudo(&[
                "nix",
                "run",
                "nixpkgs#nixos-generators",
                "--",
                "--format",
                "iso",
                "--flake",
                "/home/ehrenschwan/nixos-config#installerIso",
                "-o",
                "result",
            ])?;
        }
    }

    Ok(())
}
