{ pkgs, config, lib, ... }:
let
  conf = pkgs.writeScriptBin "conf" (builtins.readFile ./conf.rs);
  dev = pkgs.writeScriptBin "dev" (builtins.readFile ./dev.rs);
in with lib; {
  options = {
    eswn.scripts = {
      dev.enable = mkOption {
        type = types.bool;
        default = false;
        description = "Enable dev script";
      };
      conf.enable = mkOption {
        type = types.bool;
        default = false;
        description = "Enable conf script";
      };
    };
  };

  config = {
    home.packages = [ ] ++ lib.optionals config.eswn.scripts.dev.enable [ dev ]
      ++ lib.optionals config.eswn.scripts.conf.enable [ conf ];
  };
}
