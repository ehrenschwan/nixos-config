{ osConfig, lib, ... }: {
  programs.ssh = {
    enable = true;
    matchBlocks = {
      # git
      "git.ehrenschwan.org" = {
        user = "ehrenschwan";
        port = 222;
        hostname = "git.ehrenschwan.org";
        identityFile = "/home/ehrenschwan/.ssh/${osConfig.eswn.host}-git";
      };
      "git.mondlicht.org" = {
        user = "ehrenschwan";
        hostname = "git.mondlicht.org";
        identityFile = "/home/ehrenschwan/.ssh/${osConfig.eswn.host}-git";
      };
      "gitlab.com" = {
        user = "ehrenschwan";
        hostname = "gitlab.com";
        identityFile = "/home/ehrenschwan/.ssh/${osConfig.eswn.host}-git";
      };

      # workstations
      "legion.local" = lib.mkIf (osConfig.eswn.host != "legion") {
        user = "ehrenschwan";
        hostname = "192.168.178.19";
        identityFile = "/home/ehrenschwan/.ssh/${osConfig.eswn.host}-host";
      };
      "tp.local" = lib.mkIf (osConfig.eswn.host != "tp") {
        user = "ehrenschwan";
        hostname = "192.168.178.18";
        identityFile = "/home/ehrenschwan/.ssh/${osConfig.eswn.host}-host";
      };

      # server
      "adguard.local" = {
        user = "root";
        hostname = "192.168.178.3";
        identityFile = "/home/ehrenschwan/.ssh/${osConfig.eswn.host}-host";
      };
      "docker.local" = {
        user = "root";
        hostname = "192.168.178.7";
        identityFile = "/home/ehrenschwan/.ssh/${osConfig.eswn.host}-host";
      };
      "hetzner.remote" = {
        user = "root";
        hostname = "49.13.11.23";
        identityFile = "/home/ehrenschwan/.ssh/${osConfig.eswn.host}-host";
      };
      "hetzner.local" = {
        user = "root";
        hostname = "192.168.178.204";
        identityFile = "/home/ehrenschwan/.ssh/${osConfig.eswn.host}-host";
      };
      "gitlab.local" = {
        user = "root";
        hostname = "192.168.178.8";
        identityFile = "/home/ehrenschwan/.ssh/${osConfig.eswn.host}-host";
      };
    };
  };
}
