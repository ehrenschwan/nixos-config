{ config, osConfig, lib, ... }:
let
  home = config.home.homeDirectory;
  configPath = "${home}/.config";
  dotfilesPath = "${home}/nixos-config/shared/dotfiles";
  programs = [ "nvim" "zellij" ]
    ++ lib.optionals osConfig.eswn.de.enable [ "waybar" "rofi" "swaync" ];
in {
  systemd.user.tmpfiles.rules =
    map (cfg: "L+ ${configPath}/${cfg} - - - - ${dotfilesPath}/${cfg}")
    programs;
  xdg.configFile."yamlfmt/yamlfmt.yml".text = ''
    formatter:
      type: basic
      retain_line_breaks_single: true
  '';
}
