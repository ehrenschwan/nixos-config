{ config, osConfig, lib, pkgs, ... }:
with lib; {
  options = {
    eswn.de.termSize = mkOption {
      type = types.float;
      example = 14.0;
      default = 17.0;
      description = "The font sizing of the terminal";
    };
  };

  config = mkIf osConfig.eswn.de.enable {
    # As an alternative
    home.packages = with pkgs; [ alacritty ];

    programs.wezterm = {
      enable = true;
      extraConfig = # lua
        ''
          -- Pull in the wezterm API
          local wezterm = require("wezterm")

          local config = {}

          -- this is for better error messages
          if wezterm.config_builder then
            config = wezterm.config_builder()
          end

          return {
            table.unpack(config),
            color_scheme = "GruvboxDark",
            enable_tab_bar = false,
            enable_wayland = false,
            front_end = "WebGpu",
            audible_bell = "Disabled",
            window_padding = {
              left = 0,
              right = 0,
              top = 0,
              bottom = 0,
            },
            font = wezterm.font("JetBrainsMono NFM", {
              weight = "Regular",
              stretch = "Normal",
              style = "Normal",
            }),
            font_size = ${toString config.eswn.de.termSize},
            font_rules = {
              {
                intensity = "Bold",
                italic = false,
                font = wezterm.font("JetBrainsMono NFM", {
                  weight = "Bold",
                  stretch = "Normal",
                  style = "Normal",
                }),
              },
              {
                intensity = "Bold",
                italic = true,
                font = wezterm.font("JetBrainsMono NFM", {
                  weight = "Bold",
                  stretch = "Normal",
                  style = "Italic",
                }),
              },
              {
                intensity = "Normal",
                italic = true,
                font = wezterm.font("JetBrainsMono NFM", {
                  weight = "Regular",
                  stretch = "Normal",
                  style = "Italic",
                }),
              },
            },
            window_padding = {
              top = 1,
            },
            disable_default_key_bindings = true,
            keys = {
              { key = "v", mods = "SHIFT|CTRL", action = wezterm.action({ PasteFrom = "Clipboard" }) },
            },
          }
        '';
    };
  };
}
