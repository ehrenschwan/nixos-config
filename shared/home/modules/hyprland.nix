{ config, osConfig, lib, pkgs, ... }:
with lib; {
  options = {
    eswn.de = {
      monitors = mkOption {
        type = types.listOf (types.submodule {
          options = {
            name = mkOption {
              type = types.str;
              example = "eDP-1";
            };
            width = mkOption {
              type = types.int;
              example = 1920;
            };
            height = mkOption {
              type = types.int;
              example = 1080;
            };
            refreshRate = mkOption {
              type = types.int;
              default = 60;
            };
            x = mkOption {
              type = types.int;
              default = 0;
            };
            y = mkOption {
              type = types.int;
              default = 0;
            };
            enabled = mkOption {
              type = types.bool;
              default = true;
            };
          };
        });
        default = [ ];
      };
      wallpaperCmds = mkOption {
        type = types.listOf types.str;
        default = [ ];
        description = "The keymap to use";
      };
      kbVariant = mkOption {
        type = types.str;
        default = "";
        description = "The keymap to use";
      };
    };
  };

  config = mkIf osConfig.eswn.de.enable {
    xdg.portal = {
      enable = true;
      configPackages = [ pkgs.xdg-desktop-portal-hyprland ];
      extraPortals = [ pkgs.xdg-desktop-portal-hyprland ];
    };

    wayland.windowManager.hyprland = {
      enable = true;
      settings = {
        exec-once = [
          "waybar"
          "exec swaync"
          "swww init"
          "nm-applet"
          "nextcloud"
          "dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP"
        ] ++ config.eswn.de.wallpaperCmds;
        monitor = map (m:
          let
            resolution = "${toString m.width}x${toString m.height}@${
                toString m.refreshRate
              }";
            position = "${toString m.x}x${toString m.y}";
          in "${m.name},${
            if m.enabled then "${resolution},${position},1" else "disable"
          }") (config.eswn.de.monitors);
        input = {
          kb_layout = "us";
          follow_mouse = 1;
          touchpad = { natural_scroll = false; };
          sensitivity = 0;
          kb_variant =
            lib.mkIf (config.eswn.de.kbVariant != "") config.eswn.de.kbVariant;
        };
        env = [
          "HYPRCURSOR_SIZE,32"
          "XDG_SESSION_TYPE,wayland"
          "WLR_NO_HARDWARE_CURSORS,1"
        ];
        general = {
          gaps_in = 3;
          gaps_out = 4;
          border_size = 2;
          "col.active_border" = "rgba(98971aFF)";
          "col.inactive_border" = "rgba(282828FF)";
        };
        decoration = { rounding = 3; };
        animations = {
          enabled = true;
          animation = [
            "windows, 1, 2, default"
            "windowsOut, 1, 2, default"
            "border, 1, 4, default"
            "fade, 1, 8, default"
            "workspaces, 1, 2, default"
          ];
        };
        "$mainMod" = "SUPER";
        "$secondMod" = "SUPER_SHIFT";
        bind = [
          "$mainMod, t, exec, wezterm"
          "$mainMod, w, killactive,"
          "$mainMod, r, exec, rofi -show run -show-icons"
          "$mainMod, b, exec, firefox"
          "$mainMod, n, exec, swaync-client -t -sw"
          "$secondMod, q, exec, hyprctl dispatch exit"
          "$secondMod, l, exec, swaylock"

          ''
            $mainMod, s, exec, grim -g "$(slurp -o)" $HOME/pictures/screenshots/$(date +"%Y-%m-%dT%H:%M:%S")_grim.png
          ''

          "$mainMod, h, movefocus, l"
          "$mainMod, j, movefocus, r"
          "$mainMod, k, movefocus, u"
          "$mainMod, l, movefocus, d"

          # Switch to workspaces
          "$mainMod, 1, workspace, 1"
          "$mainMod, 2, workspace, 2"
          "$mainMod, 3, workspace, 3"
          "$mainMod, 4, workspace, 4"
          "$mainMod, 5, workspace, 5"
          "$mainMod, 6, workspace, 6"
          "$mainMod, 7, workspace, 7"
          "$mainMod, 8, workspace, 8"
          "$mainMod, 9, workspace, 9"
          "$mainMod, 0, workspace, 10"

          # Move window to workspaces
          "$mainMod SHIFT, 1, movetoworkspace, 1"
          "$mainMod SHIFT, 2, movetoworkspace, 2"
          "$mainMod SHIFT, 3, movetoworkspace, 3"
          "$mainMod SHIFT, 4, movetoworkspace, 4"
          "$mainMod SHIFT, 5, movetoworkspace, 5"
          "$mainMod SHIFT, 6, movetoworkspace, 6"
          "$mainMod SHIFT, 7, movetoworkspace, 7"
          "$mainMod SHIFT, 8, movetoworkspace, 8"
          "$mainMod SHIFT, 9, movetoworkspace, 9"
          "$mainMod SHIFT, 0, movetoworkspace, 10"
        ];
      };
    };
  };
}
