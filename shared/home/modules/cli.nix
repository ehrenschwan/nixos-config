{ pkgs, ... }: {
  home.packages = with pkgs; [
    bind
    jq
    bat
    ripgrep
    httpie
    btop
    lazygit
    lazydocker
    eza
    pfetch
    unzip
    tokei
    zellij
    direnv
    tldr
    wireguard-tools
    killall
  ];
}
