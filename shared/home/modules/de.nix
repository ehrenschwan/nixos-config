{ pkgs, config, osConfig, lib, ... }:
with lib; {
  config = mkIf osConfig.eswn.de.enable {
    home.packages = with pkgs; [
      wl-clipboard
      alsa-utils
      networkmanagerapplet
      rofi-wayland
      swww
      (waybar.overrideAttrs (oldAttrs: {
        mesonFlags = oldAttrs.mesonFlags ++ [ "-Dexperimental=true" ];
      }))
      libnotify
      swaynotificationcenter
    ];

    home.pointerCursor = let
      getFrom = url: hash: name: {
        gtk.enable = true;
        size = 24;
        name = name;
        package = pkgs.runCommand "moveUp" { } ''
          mkdir -p $out/share/icons
          ln -s ${
            pkgs.fetchzip {
              url = url;
              hash = hash;
            }
          } $out/share/icons/${name}
        '';
      };
    in getFrom
    "https://github.com/ful1e5/Bibata_Cursor/releases/download/v2.0.6/Bibata-Modern-Classic.tar.xz"
    "sha256-jpEuovyLr9HBDsShJo1efRxd21Fxi7HIjXtPJmLQaCU="
    "Bibita-Modern-Classic";

    gtk = {
      enable = true;
      theme = {
        name = "Gruvbox";
        package = pkgs.gruvbox-gtk-theme;
      };
      iconTheme = {
        name = "Gruvbox Dark Icons Theme";
        package = pkgs.gruvbox-dark-icons-gtk;
      };
    };

    # Now symlink the `~/.config/gtk-4.0/` folder declaratively:
    xdg.configFile = {
      "gtk-4.0/assets".source =
        "${config.gtk.theme.package}/share/themes/${config.gtk.theme.name}/gtk-4.0/assets";
      "gtk-4.0/gtk.css".source =
        "${config.gtk.theme.package}/share/themes/${config.gtk.theme.name}/gtk-4.0/gtk.css";
      "gtk-4.0/gtk-dark.css".source =
        "${config.gtk.theme.package}/share/themes/${config.gtk.theme.name}/gtk-4.0/gtk-dark.css";
    };
  };
}
