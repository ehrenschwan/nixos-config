{ pkgs, config, lib, osConfig, ... }:
with lib; {
  options = {
    eswn.de.guiPrograms = {
      enable = mkOption {
        type = types.bool;
        default = false;
        description = "Enable GUI programs";
      };
    };
  };

  config = mkIf config.eswn.de.guiPrograms.enable {
    home.packages = with pkgs;
      [
        # Browsers
        firefox
        chromium

        nextcloud-client

        # Writing
        obsidian
        libreoffice-qt

        # Graphics
        vlc
        inkscape
        gimp
        figma-linux
        freecad

        # Development
        beekeeper-studio

        # Listening 
        spotify
        pocket-casts

        # System
        nautilus
        obs-studio
        obs-studio-plugins.wlrobs
        grim
        slurp

        # Communication
        element-desktop-wayland
        thunderbird
      ] ++ lib.optionals osConfig.eswn.wireshark.enable [ wireshark ];
  };
}
