{ pkgs, config, lib, ... }:
with lib; {
  options = {
    eswn.de.ms-teams = {
      enable = mkOption {
        type = types.bool;
        default = false;
        description = "Enable ms-teams (teams-for-linux)";
      };
    };
  };

  config = mkIf config.eswn.de.ms-teams.enable {
    home.packages = with pkgs; [ teams-for-linux ];

    xdg.configFile."teams-for-linux/Preferences".text = ''
      {
        "secure": "true",
        "spellcheck": {
          "dictionaries": [
            "en-US"
          ],
          "dictionary": ""
        }
      }
    '';
  };
}
