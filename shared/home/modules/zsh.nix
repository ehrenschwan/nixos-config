{ config, osConfig, lib, ... }:
with lib; {
  options = {
    eswn.cli.zsh = {
      aliases = mkOption {
        default = { };
        example = literalExpression ''
          {
            ll = "ls -l";
            ".." = "cd ..";
          }
        '';
        description = ''
          An attribute set that maps aliases (the top level attribute names in
          this option) to command strings or directly to build outputs.
        '';
        type = types.attrsOf types.str;
      };
    };
  };

  config = lib.mkIf osConfig.eswn.zsh.enable {
    programs.zsh = {
      enable = true;
      autosuggestion.enable = true;
      enableCompletion = true;
      syntaxHighlighting.enable = true;
      shellAliases = {
        ls = "eza";
        la = "eza -la";
        b = "bat";
        ltree = ''eza -la --tree --git-ignore --ignore-glob=".git|target"'';
        v = "nvim";
        cl = "clear && pfetch";
        ".." = "cd ..";
        h = "cd ~";
        lg = "lazygit";
        ld = "lazydocker";
        j = "just";
        vu = "amixer set Master 5%+";
        vd = "amixer set Master 5%-";
        vm = "amixer sset Master toggle";
      } // config.eswn.cli.zsh.aliases;
      history = {
        size = 10000;
        path = "${config.xdg.dataHome}/zsh/history";
      };
      initExtra = ''
        pfetch
        eval "$(direnv hook zsh)"
      '';
    };
  };
}
