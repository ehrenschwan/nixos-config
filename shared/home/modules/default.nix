{ ... }: {
  imports = [
    ./cli.nix
    ./coding.nix
    ./de.nix
    ./dotfiles.nix
    ./fonts.nix
    ./gaming.nix
    ./gui.nix
    ./hyprland.nix
    ./scripts
    ./ssh.nix
    ./starship.nix
    ./swaylock.nix
    ./teams.nix
    ./virtualisation.nix
    ./wezterm.nix
    ./zsh.nix
  ];
}
