{ lib, osConfig, ... }:
with lib; {
  config = mkIf osConfig.eswn.virtualisation.vms {
    dconf.settings = {
      "org/virt-manager/virt-manager/connections" = {
        autoconnect = [ "qemu:///system" ];
        uris = [ "qemu:///system" ];
      };
    };
  };
}
