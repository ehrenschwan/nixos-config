# My nixos-config

This is my nixos config.

## Configuration Options

```nix
eswn = {
    host = "hostname";
    allowedTCPPorts = [ 4173 5173 ];
    systemdBoot.enable = true;
    # or
    # grubBoot.enable = true;
    amdgpu.enable = true;
    user.ehrenschwan = {
        enable = true;
        authorizedKeys = [ /* some ssh key */ ];
    };
    user.root.authorizedKeys = [ /* some ssh key */ ];
    greetd.enable = true;
    zsh.enable = true;
    de.enable = true;
    emulation.aarch.enable = true;
    virtualisation = {
        containers = true;
        vms = true;
    };
    wireshark.enable = true;
};
```

These options are available in home manager modules using `osConfig`.

## Home Manager Options

```nix
eswn = {
    cli = {
        zsh.aliases = {
            alias = "...";
        };
    };
    scripts = {
        # for zellij sessions
        dev.enable = true;
        # for switching configs
        conf.enable = true;
    };
    de = {
        guiPrograms.enable = true;
        ms-teams.enable = true;
        gaming.enable = true;
        termSize = 14.0;
        monitors = [
        {
            name = "DP-1";
            width = 1920;
            height = 1080;
            refreshRate = 144;
            x = 0;
            y = 0;
            enabled = true;
        }
        ];
        wallpaperCmds = [
          "swww img -o DP-1 ${../../wallpapers/rivermountain_middle.jpg} "
        ];
    };
};
```
