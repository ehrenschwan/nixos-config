{ ... }: {
  services.adguardhome = {
    enable = true;
    host = "192.168.178.3";
    port = 443;
    openFirewall = true;
    settings = {
      dns = {
        bind_hosts = [ "127.0.0.1" "::1" "192.168.178.3" ];
        upstream_dns = [
          "https://dns10.quad9.net/dns-query"
          "https://dns.cloudflare.com/dns-query"
        ];
        all_servers = true;
        bootstrap_dns =
          [ "9.9.9.10" "149.112.112.10" "2620:fe::10" "2620:fe::fe:10" ];
      };
      filtering.rewrites = [
        {
          domain = "fritz.box";
          answer = "192.168.178.1";
        }
        {
          domain = "ehrenschwan.org";
          answer = "192.168.178.4";
        }
        {
          domain = "*.ehrenschwan.org";
          answer = "192.168.178.4";
        }
        {
          domain = "cloud.ehrenschwan.org";
          answer = "192.168.178.204";
        }
      ];
    };
  };
}
