{ modulesPath, ... }: {
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
    (modulesPath + "/profiles/qemu-guest.nix")

    # base & shared modules
    ../../shared/configuration

    # host modules
    ./modules
  ];

  eswn = {
    host = "adguard";
    allowedTCPPorts = [ 53 3000 ];
    allowedUDPPorts = [ 53 ];
    systemdBoot.enable = true;
    user.root.authorizedKeys = [
      (builtins.readFile ../../ssh_keys/legion-host.pub)
      (builtins.readFile ../../ssh_keys/tp-host.pub)
    ];
  };

  networking = {
    interfaces = {
      ens18 = {
        useDHCP = true;
        ipv4.addresses = [{
          address = "192.168.178.3";
          prefixLength = 24;
        }];
      };
    };
    defaultGateway = {
      address = "192.168.178.1";
      interface = "ens18";
    };
  };
}
