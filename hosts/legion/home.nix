{ ... }: {

  imports = [ ../../shared/home ];

  home.username = "ehrenschwan";
  home.homeDirectory = "/home/ehrenschwan";

  eswn = {
    cli = { zsh.aliases = { sw = "conf switch legion"; }; };
    scripts = {
      dev.enable = true;
      conf.enable = true;
    };
    de = {
      guiPrograms.enable = true;
      ms-teams.enable = true;
      gaming.enable = true;
      termSize = 14.0;
      monitors = [
        {
          name = "DP-3";
          width = 1920;
          height = 1080;
          refreshRate = 60;
          x = 0;
          y = 0;
          enabled = true;
        }
        {
          name = "DP-1";
          width = 1920;
          height = 1080;
          refreshRate = 144;
          x = 1920;
          y = 0;
          enabled = true;
        }
        {
          name = "DP-2";
          width = 1920;
          height = 1080;
          refreshRate = 144;
          x = 3840;
          y = 0;
          enabled = true;
        }
      ];
      wallpaperCmds = [
        "swww img -o DP-3 ${../../wallpapers/rivermountain_left.jpg} "
        "swww img -o DP-1 ${../../wallpapers/rivermountain_middle.jpg} "
        "swww img -o DP-2 ${../../wallpapers/rivermountain_right.jpg} "
      ];
    };
  };
}

