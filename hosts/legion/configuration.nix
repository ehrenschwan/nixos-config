{ ... }: {
  imports = [
    # hardware
    ./hardware-configuration.nix

    # base & shared modules
    ../../shared/configuration

    # vpn
    ./modules
  ];

  eswn = {
    host = "legion";
    allowedTCPPorts = [ 4173 5173 ];
    systemdBoot.enable = true;
    amdgpu.enable = true;
    user.ehrenschwan = {
      enable = true;
      authorizedKeys = [ (builtins.readFile ../../ssh_keys/tp-host.pub) ];
    };
    greetd.enable = true;
    zsh.enable = true;
    de.enable = true;
    emulation.aarch.enable = true;
    virtualisation = {
      containers = true;
      vms = true;
    };
    wireshark.enable = true;
    printing.enable = true;
  };
}
