{ ... }: {
  networking.wg-quick.interfaces = {
    schwan = {
      address = [ "192.168.1.203/24" ];
      dns = [ "192.168.1.2" "192.168.1.1" ];
      privateKeyFile = "/home/ehrenschwan/.wireguard/schwan/private_key";

      autostart = false;
      peers = [{
        publicKey = "bEYVTs6LWEW59pfIVPYu/fxzKyuem4FIQH0Czl2lwDo=";
        presharedKeyFile = "/home/ehrenschwan/.wireguard/schwan/preshared_key";
        allowedIPs = [ "192.168.1.0/24" "0.0.0.0/0" ];
        endpoint = "yezgq406uj4ggs92.myfritz.net:53893";
        persistentKeepalive = 25;
      }];
    };
  };
}
