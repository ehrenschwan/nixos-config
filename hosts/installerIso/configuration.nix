{ modulesPath, ... }: {
  imports = [
    "${modulesPath}/installer/cd-dvd/installation-cd-minimal.nix"
    ../../shared/configuration
  ];

  eswn = {
    host = "installer";
    user.root.authorizedKeys = [
      (builtins.readFile ../../ssh_keys/legion-host.pub)
      (builtins.readFile ../../ssh_keys/tp-host.pub)
    ];
  };

  # This has to be set for the installation-cd-minimal.nix to work
  nixpkgs.hostPlatform = "x86_64-linux";

  # This is set to true in nixos/modules/profiles/installation-device.nix
  networking.wireless.enable = false;

  # EFI booting
  isoImage.makeEfiBootable = true;

  # USB booting
  isoImage.makeUsbBootable = true;
}
