{ modulesPath, ... }: {
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
    (modulesPath + "/profiles/qemu-guest.nix")

    # base & shared modules
    ../../shared/configuration

    # host modules
    ./modules
  ];

  eswn = {
    host = "gitlab";
    allowedTCPPorts = [ 80 ];
    systemdBoot.enable = true;
    user.root.authorizedKeys = [
      (builtins.readFile ../../ssh_keys/legion-host.pub)
      (builtins.readFile ../../ssh_keys/tp-host.pub)
    ];
    virtualisation = { containers = true; };
  };

  networking = {
    nameservers = [ "192.168.178.3" ];
    interfaces = {
      ens18 = {
        useDHCP = true;
        ipv4.addresses = [{
          address = "192.168.178.8";
          prefixLength = 24;
        }];
      };
    };
    defaultGateway = {
      address = "192.168.178.1";
      interface = "ens18";
    };
  };
}
