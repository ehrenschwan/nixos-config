{ pkgs, ... }: {
  fileSystems."/mnt/gitlab_backups" = {
    fsType = "nfs";
    device = "192.168.178.6:/mnt/RaidZ2-Pool/NFS/gitlab_backups";
    options = [ "x-systemd.automount" "noauto" "rw" ];
  };
}
