{ pkgs, age, config, ... }: {
  age.secrets = let
    file = file_name: {
      file = ../../../secrets/gitlab/${file_name};
      owner = "gitlab";
      group = "gitlab";
    };
  in {
    dbPw = file "dbPw.age";
    rootPw = file "rootPw.age";
    secret = file "secret.age";
    otpSecret = file "otpSecret.age";
    dbSecret = file "dbSecret.age";
    jws = file "jws.age";
  };

  services.gitlab =
    let secret = secret_name: config.age.secrets.${secret_name}.path;
    in {
      enable = true;
      packages.gitlab = pkgs.gitlab;
      host = "gitlab.ehrenschwan.org";
      https = true;
      port = 443;
      databasePasswordFile = secret "dbPw";
      initialRootPasswordFile = secret "rootPw";
      secrets = {
        secretFile = secret "secret";
        otpFile = secret "otpSecret";
        dbFile = secret "dbSecret";
        jwsFile = secret "jws";
      };

      backup.startAt = "03:00";
      backup.uploadOptions = {
        connection = {
          provider = "local";
          local_root = "/mnt/gitlab_backups";
        };
        remote_directory = "";
        backup_keep_time = 604800;
      };
    };
}
