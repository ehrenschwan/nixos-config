{ ... }: {
  services.caddy = {
    enable = true;
    virtualHosts."http://".extraConfig = ''
      reverse_proxy unix//run/gitlab/gitlab-workhorse.socket
    '';
  };
}
