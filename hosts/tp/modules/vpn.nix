{ ... }: {
  networking.wg-quick.interfaces = {
    home = {
      address = [ "192.168.178.203/24" ];
      dns = [ "192.168.178.3" "192.168.178.1" ];
      privateKeyFile = "/home/ehrenschwan/.wireguard/home/private_key";

      autostart = false;
      peers = [{
        publicKey = "TQRG9fFLUiav9c5zlnFYU6EOV5xoQmYIGtWd9wtDhA8=";
        presharedKeyFile = "/home/ehrenschwan/.wireguard/home/preshared_key";
        allowedIPs = [ "192.168.178.0/24" "0.0.0.0/0" ];
        endpoint = "r7jh6a6131fa4vz6.myfritz.net:56938";
        persistentKeepalive = 25;
      }];
    };
    schwan = {
      address = [ "192.168.1.202/24" ];
      dns = [ "192.168.1.2" "192.168.1.1" ];
      privateKeyFile = "/home/ehrenschwan/.wireguard/schwan/private_key";

      autostart = false;
      peers = [{
        publicKey = "bEYVTs6LWEW59pfIVPYu/fxzKyuem4FIQH0Czl2lwDo=";
        presharedKeyFile = "/home/ehrenschwan/.wireguard/schwan/preshared_key";
        allowedIPs = [ "192.168.1.0/24" "0.0.0.0/0" ];
        endpoint = "yezgq406uj4ggs92.myfritz.net:53893";
        persistentKeepalive = 25;
      }];
    };
  };
}
