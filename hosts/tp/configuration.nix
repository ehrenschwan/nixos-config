{ pkgs, ... }: {
  imports = [
    # hardware
    ./hardware-configuration.nix

    # base & shared modules
    ../../shared/configuration

    # host modules
    ./modules
  ];

  # Fingerprint configuration
  environment.systemPackages = with pkgs; [ fprintd ];
  services.fprintd = { tod.enable = true; };
  security.pam.services.swaylock = { fprintAuth = true; };

  console.keyMap = "colemak";

  eswn = {
    host = "tp";
    allowedTCPPorts = [ 4173 5173 ];
    systemdBoot.enable = true;
    amdgpu.enable = true;
    user.ehrenschwan = {
      enable = true;
      authorizedKeys = [ (builtins.readFile ../../ssh_keys/legion-host.pub) ];
    };
    greetd.enable = true;
    zsh.enable = true;
    de.enable = true;
    emulation.aarch.enable = true;
    virtualisation = {
      containers = true;
      vms = true;
    };
    wireshark.enable = true;
    printing.enable = true;
  };
}
