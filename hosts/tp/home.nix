{ ... }: {

  imports = [ ../../shared/home ];

  home.username = "ehrenschwan";
  home.homeDirectory = "/home/ehrenschwan";

  eswn = {
    cli = { zsh.aliases = { sw = "conf switch tp"; }; };
    scripts = {
      dev.enable = true;
      conf.enable = true;
    };
    de = {
      guiPrograms.enable = true;
      gaming.enable = true;
      monitors = [{
        name = "eDP-1";
        width = 1920;
        height = 1080;
        refreshRate = 60;
        x = 0;
        y = 0;
        enabled = true;
      }];
      wallpaperCmds = [ "swww img ${../../wallpapers/future-town.jpg}" ];
      termSize = 17.0;
      kbVariant = "colemak";
    };
  };

}

