{ ... }: {
  networking.wg-quick.interfaces = {
    home = {
      address = [ "192.168.178.204/24" ];
      dns = [ "192.168.178.3" "192.168.178.1" ];
      privateKeyFile = "/root/.wireguard/home/private_key";

      autostart = false;
      peers = [{
        publicKey = "TQRG9fFLUiav9c5zlnFYU6EOV5xoQmYIGtWd9wtDhA8=";
        presharedKeyFile = "/root/.wireguard/home/preshared_key";
        allowedIPs = [ "192.168.178.0/24" ];
        endpoint = "r7jh6a6131fa4vz6.myfritz.net:56938";
        persistentKeepalive = 25;
      }];
    };
  };
}
