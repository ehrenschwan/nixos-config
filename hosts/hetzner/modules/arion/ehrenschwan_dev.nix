{ ... }: {
  services = {
    ehrenschwan_dev = {
      service = {
        container_name = "ehrenschwan_dev";
        image = "git.ehrenschwan.org/ehrenschwan/ehrenschwan_dev:1.2.0";
        ports = [ "127.0.0.1:5050:5050" ];
      };
    };
  };
}
