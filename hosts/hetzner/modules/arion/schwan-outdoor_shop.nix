{ ... }: {
  services = {
    schwan-outdoor_shop = {
      service = {
        container_name = "schwan-outdoor_shop";
        image = "git.ehrenschwan.org/ehrenschwan/schwan-outdoor_shop:1.0.6";
        ports = [ "127.0.0.1:5051:5050" ];
      };
    };
  };
}
