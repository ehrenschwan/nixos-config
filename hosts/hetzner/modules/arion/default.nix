{ ... }: {
  virtualisation.arion = let
    projectsFromName = projects:
      builtins.listToAttrs (map (x: {
        name = x;
        value = { settings = { imports = [ ./${x}.nix ]; }; };
      }) projects);
  in {
    backend = "podman-socket";
    projects = projectsFromName [ "ehrenschwan_dev" "schwan-outdoor_shop" ];
  };
}
