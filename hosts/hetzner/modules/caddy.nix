{ ... }: {
  services.caddy = {
    enable = true;
    virtualHosts."ehrenschwan.dev".extraConfig = ''
      reverse_proxy http://127.0.0.1:5050
    '';
    virtualHosts."schwan-outdoor.shop".extraConfig = ''
      reverse_proxy http://127.0.0.1:5051
    '';
    virtualHosts."cloud.ehrenschwan.org".extraConfig = ''
      tls {
        protocols tls1.2
      }

      request_body {
        max_size 2G
      }

      header {
        Host cloud.ehrenschwan.org
        Strict-Transport-Security "max-age=31536000; includeSubdomains; preload;"
      }

      reverse_proxy http://192.168.178.4:8080 {
      }
    '';
  };
}
