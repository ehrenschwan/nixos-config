{ modulesPath, ... }: {
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
    (modulesPath + "/profiles/qemu-guest.nix")

    # base & shared modules
    ../../shared/configuration

    # host modules
    ./modules
  ];

  eswn = {
    host = "hetzner";
    allowedTCPPorts = [ 80 443 ];
    grubBoot.enable = true;
    user.root.authorizedKeys = [
      (builtins.readFile ../../ssh_keys/legion-host.pub)
      (builtins.readFile ../../ssh_keys/tp-host.pub)
    ];
    virtualisation = { containers = true; };
  };

  systemd.network = {
    enable = true;
    networks."10-wan" = {
      matchConfig.Name = "enp1s0";
      networkConfig.DHCP = "ipv4";
      address = [ "2a01:4f8:c17:edef::/64" ];
      routes = [{ Gateway = "fe80::1"; }];
    };
  };
}
