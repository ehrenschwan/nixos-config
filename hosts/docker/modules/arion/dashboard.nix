{ ... }: {
  services = {
    dashboard = {
      service = {
        image = "git.ehrenschwan.org/ehrenschwan/dashboard:1.0.6";
        container_name = "dashboard";
        restart = "unless-stopped";
        ports = [ "5050:5050" ];
      };
    };
  };
}
