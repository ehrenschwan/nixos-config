{ ... }: {
  docker-compose.volumes = { vaultwarden = { }; };
  services = {
    vaultwarden = {
      service = {
        image = "vaultwarden/server:latest";
        container_name = "vaultwarden";
        restart = "unless-stopped";
        ports = [ "81:80" ];
        volumes = [ "vaultwarden:/data" ];
      };
    };
  };
}
