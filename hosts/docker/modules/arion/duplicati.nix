{ ... }: {
  docker-compose.volumes = {
    duplicati = { };
    vaultwarden_vaultwarden = { external = true; };
  };
  services = {
    duplicati = {
      service = {
        image = "linuxserver/duplicati:2.0.8";
        container_name = "duplicati";
        restart = "unless-stopped";
        ports = [ "8200:8200" ];
        volumes = [
          "duplicati:/config"
          "vaultwarden_vaultwarden:/source/vaultwarden_vaultwarden"
        ];
        environment = {
          PUID = 0;
          PGID = 0;
          TZ = "Europe/Berlin";
        };
      };
    };
  };
}
