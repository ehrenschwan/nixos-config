{ ... }: {
  services = {
    episode_randomizer = {
      service = {
        image = "git.ehrenschwan.org/ehrenschwan/episode_randomizer:1.0.0";
        container_name = "episode_randomizer";
        restart = "unless-stopped";
        ports = [ "5051:5050" ];
      };
    };
  };
}
