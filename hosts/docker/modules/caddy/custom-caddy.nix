{ pkgs, config, plugins, stdenv, lib, ... }:
let
  caddySrc = pkgs.fetchFromGitHub {
    owner = "caddyserver";
    repo = "caddy";
    rev = "v2.7.6";
    sha256 = "sha256-th0R3Q1nGT0q5PGOygtD1/CpJmrT5TYagrwQR4t/Fvg=";
  };
in pkgs.buildGoModule {
  noCheck = true;
  pname = "cloudflare-caddy";
  version = caddySrc.rev;
  subPackages = [ "cmd/caddy" ];
  src = caddySrc;
  patches = [ ./001-cloudflare.patch ];

  ldflags = [
    "-X github.com/caddyserver/caddy/v2.CustomVersion=${caddySrc.rev}-cloudflare"
  ];

  # set to lib.fakeSha256 to get the new one
  # vendorHash = "${lib.fakeSha256}";
  vendorHash = "sha256-dnKAwOrQkICkUVsyWJO+o2N4HcImLaL+fPyq8hUd5/8=";

  checkPhase = "";
}
