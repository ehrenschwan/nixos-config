{ age, pkgs, config, ... }: {
  # this should become a module
  services.caddy = {
    enable = true;
    package = (pkgs.callPackage ./custom-caddy.nix {
      plugins = [ "github.com/caddy-dns/cloudflare" ];
    });
    virtualHosts."*.ehrenschwan.org, ehrenschwan.org" = {
      extraConfig = ''
        tls {
          dns cloudflare {env.CLOUDFLARE_API_TOKEN}
        }

        @rand host rand.ehrenschwan.org
        handle @rand {
          reverse_proxy 127.0.0.1:5051
        }
      '';
      logFormat =
        "output file ${config.services.caddy.logDir}/access-ehrenschwan.org.log";
    };
  };

  age.secrets.caddy-env-file.file = ../../../../secrets/caddy-env-file.age;

  systemd.services.caddy = {
    serviceConfig = {
      # Required to use ports < 1024
      AmbientCapabilities = "cap_net_bind_service";
      CapabilityBoundingSet = "cap_net_bind_service";
      EnvironmentFile = config.age.secrets."caddy-env-file".path;
      TimeoutStartSec = "5m";
    };
  };
}
