let
  tp-ehrenschwan = builtins.readFile ../ssh_keys/tp-host.pub;
  legion-ehrenschwan = builtins.readFile ../ssh_keys/legion-host.pub;
  users = [ tp-ehrenschwan legion-ehrenschwan ];

  tp =
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOXP+Up91SObMHU4a6D+mgxDcyYOQL/Td+TDI6UosrR";
  legion =
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKjICkY4fEL1ccPwUlT00Os9KPrgByStzWq48onYoRBI";
  docker =
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINUY/HPiUm49V13Byy8WfZQ1XPfyE8i4pMe8Y88+d7WR";
  hetzner =
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFPbTpg9M4cOUQBq24M4odW4DEDkizGnUB7RYnRQPwVM";
  adguard =
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEQS3NHc2tGOwWjFJjRxQKvyag8aF/q6frM5/sIbRXaf";
  gitlab =
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGw2g2g4CY3VaTEz8YNXBn3EV0iDdVvAr8FybXY3l9rh";
  systems = [ tp legion docker hetzner adguard gitlab ];
in {
  "caddy-env-file.age".publicKeys = users ++ [ docker ];
  "gitlab/dbPw.age".publicKeys = users ++ [ gitlab ];
  "gitlab/rootPw.age".publicKeys = users ++ [ gitlab ];
  "gitlab/secret.age".publicKeys = users ++ [ gitlab ];
  "gitlab/otpSecret.age".publicKeys = users ++ [ gitlab ];
  "gitlab/dbSecret.age".publicKeys = users ++ [ gitlab ];
  "gitlab/jws.age".publicKeys = users ++ [ gitlab ];
}
